import pandas as pd
import numpy as np
from nmi import Clustering
import logging

logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(levelname)s %(message)s')
logger = logging.getLogger(__name__)

def jaccard():
    pass

def file_content(filename='data/places.txt', delim=' '):
    """Read file and yiel line by line"""

    with open(filename, 'r') as f:
        return np.array([np.fromstring(line, dtype=int, sep=' ') for line in f])

def main():
    gT = pd.DataFrame(file_content('data/partitions.txt'), columns=['id', 'cluster'])

    clusters = [pd.DataFrame(filename, columns=['id', 'cluster']) for filename in [file_content('data/clustering_%i.txt' % i) for i in range(1,6)]]
    for i in range(1,6):
        logger.debug("Processing clustering %s" % i)
        documents = file_content('data/clustering_%i.txt' % i)
        clustering = pd.DataFrame(documents, columns=['id', 'cluster'])

        C = Clustering(clustering, gT)
        print(C.summary())
        #print(C.probabilitiesSummary())
        #print(C.columnTotals())
        #print(C.rowTotals())
        logger.debug("I=%f" % C.I())
        logger.debug("HC=%f" % C.HC())
        logger.debug("HT=%f" % C.HT())
        logger.info("%f %f" % (C.NMI(), C.jaccard()))
        #logging.debug("clustering_%s H=%f" % (idx, C.nmi()))
        #nmi = NMI(gT, clustering)
        #logging.debug("gT entroy = %f" % nmi.entropyGT)
        #nmi.H(clustering)
        #nmi.I(clustering)
    #    partitions = len(clustering.cluster.unique())
    #    print("Partitions in cluster => %i" % partitions)
    #    matrix = pd.DataFrame(columns=['T1', 'T2', 'T3'])
    #    #dataFrame.at[idx, self.SUPPORT] += 1 #= current_support + 1
    #    for i in range(0, len(clustering.cluster.unique()) ):
    #        r1 = list(gT[gT['cluster'] == i].id)
    #        r2 = list(clustering[clustering['cluster'] == i].id)
#   #         print("%i | %i |
    #        print(len(set(r1).intersection(r2))/300.0)
    #cluster_summary(gT)

    #for cluster in clusters:
    #    cluster_summary(cluster)


if __name__ == "__main__" : main()
