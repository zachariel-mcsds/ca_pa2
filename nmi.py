import numpy as np
import math
import pandas as pd
import logging

logger = logging.getLogger(__name__)
#/Users/zachariel/Development/python/mcs-ds/cs410/project/classification/views.py|18 col 1| logger = logging.getLogger(__name__)

def nCr(n, r):
    f = math.factorial
    return f(n) // (f(r) * f(n - r))

class Clustering:

    def __init__(self, documents, groundTruth):
        self.gT = groundTruth

        self.classTypes = list(self.gT['cluster'].unique())
        self.classTypes.sort()

        self.N = len(documents)

        types = list(documents['cluster'].unique())
        types.sort()

        self.columns = ["T%i" % i for i in self.classTypes]

        self.clusters = list()

        for id in types:
            self.clusters.append( Cluster(id, documents[documents['cluster'] == id], self.N) )


    def NMI(self):
        return self.I() / math.sqrt( self.HC() * self.HT() )

    def jaccard(self):
        """
        Jaccard coefficient = TP / (TP + FN + FP)
        """
        return self.TP() / (self.TP() + self.FN() + self.FP() )

    def probabilitiesSummary(self):
        return self.summary() / self.N

    def summary(self):
        #print(clustering[clustering['cluster'] == 0])
        summary = pd.DataFrame(columns=self.columns)
        for cluster in self.clusters:
            row = []
            for t in self.classTypes:
                docs = set(self.gT[self.gT['cluster'] == t].id)
                row.append(cluster.TP(docs))
            summary.loc[len(summary)] = row


        return summary

    def I(self):
        mi = 0
        summary = self.probabilitiesSummary()

        for idx, row in summary.iterrows():
            for column in self.columns:
                p = row[column]
                colP = self.columnTotals()[column]
                rowP = self.rowTotals()[idx]
                expP = colP * rowP

                if( p > 0 ):
                    mi += p * math.log( (p / expP), 2 )

        return mi

    def HC(self):
        """
        Cluster entropy
         -∑ P(c)*log(P(c))
        """
        logger.debug('Calculating H(c)')

        summary = self.summary()

        entropy = 0
        for row in self.rowTotals():
            if(row > 0):
                entropy += row * math.log(row, 2)

        return entropy

    def columnTotals(self):
        summary = self.probabilitiesSummary()

        totals = pd.DataFrame(columns=self.columns)
        columns = []
        for col in self.columns:
            column = summary[col]
            columns.append(column.sum())# / self.N

        totals.loc[0] = columns
        return totals

    def rowTotals(self):
        summary = self.probabilitiesSummary()

        rows = []
        for idx, row in summary.iterrows():
            rows.append( row.sum() )

        return pd.Series(rows)

    def HT(self):
        summary = self.probabilitiesSummary()

        entropy = 0
        for col in self.columnTotals().loc[0]:
            if(col > 0):
                entropy += col * math.log(col, 2)

        return entropy

    def TP(self):
        summary = self.summary()

        tp = 0
        for i, row in summary.iterrows():
            for j, col in enumerate(row):
                value = col
                tp+= value ** 2

                #if( i == j ):
                #    combinations = nCr(value, 2)
                #    tp += combinations

        return (tp - self.N) / 2

    def FN(self):
        summary = self.summary()
        fn = 0
        for i, row in summary.iterrows():
            fn += nCr(row.sum(), 2)

        return fn - self.TP()

    def FP(self):
        summary = self.summary()
        fp = 0
        for i, col in summary.iteritems():
            fp += nCr(col.sum(), 2)

        return fp - self.TP()

class Cluster:
    def __init__(self, cid, C, n):
        self.id = cid
        self.classes = C.cluster.unique()
        self.classes.sort()
        self.cluster = C
        self.N = n

    @property
    def length(self):
        return len(self.cluster)

    @property
    def ids(self):
        return set(self.cluster.id)

    def H(self):
        P = self.P()
        return -1 * P * math.log(P, 2)

    def P(self):
        return self.length / self.N

    def TP(self, documents):
        return len(documents.intersection(self.ids))

    def Pconditional(self, documents):
        return self.TP(documents) / self.N

    def Hconditional(self, documents):
        classTypes = documents.cluster.unique()
        classTypes.sort()

        entropy = 0
        for classType in classTypes:
            logger.debug("Calculate conditional entropy: Cluster[%i] Class[%i]" % (self.id, classType))
            classDocuments = set(documents[documents['cluster']== classType].id)

            probability = self.Pconditional(classDocuments)
            logger.debug("P(%i|%i) = %f" % (classType, self.id, probability))

            if(probability > 0):
                entropy += probability * math.log( probability, 2)

        entropy = -1 * self.H() * entropy

        logger.debug("H(Y|%i) = %f" % (self.id, entropy))
        return entropy
